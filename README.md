# Projeto de LAPR4 2018-18 do Grupo [XX-Y]#


# 1. Constituição do Grupo de Trabalho ###

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.	   | Nome do Aluno			    |
|--------------|------------------------------|
| **[1170000](/docs/1170000/)**  | Aluno Exemplo                |
|              | 						        |
|              | 						        |
|              | 						        |
|              | 						        |
|              | 						        |
|              | 						        |


# 2. Distribuição de Funcionalidades ###

A distribuição de requisitos/funcionalidades ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito na tabela seguinte.

| Aluno Nr.	| Semana 1 | Semana 2 | Semana 3 |
|------------|----------|----------|----------|
| [**1170000**](/docs/1170000/)| [USDemo1](/docs/1170000/USDemo1)| [USDemo2](/docs/1170000/USDemo2)| [USDemo3](/docs/1170000/USDemo3) |
|          	|          |          |          |
|          	|          |          |          |
|          	|          |          |          |
|          	|          |          |          |
|          	|          |          |          |
|          	|          |          |          |